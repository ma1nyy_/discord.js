const getRandomInt = (min, max) => {
	return min + Math.round(Math.random() * (max - min));
};

export const getRandomArrayElement = (array) => {
	return array[getRandomInt(0, array.length - 1)];
};
