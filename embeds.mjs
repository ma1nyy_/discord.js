import Discord from "discord.js";
import {
	boisServer,
	buildServer,
	help,
	mainServer,
	watchedServer,
} from "./config.mjs";

export const embedOnline = new Discord.MessageEmbed()
	.setColor("#1ade00")
	.setAuthor(
		watchedServer,
		"https://cdn.discordapp.com/attachments/777244698393575444/784821735425966090/rudit.png"
	)
	.setTitle("Server status")
	.setDescription("ONLINE ✅")
	.setTimestamp(new Date());

export const embedOffline = new Discord.MessageEmbed()
	.setColor("#ff0000")
	.setAuthor(
		watchedServer,
		"https://cdn.discordapp.com/attachments/777244698393575444/784821735425966090/rudit.png"
	)
	.setTitle("Server status")
	.setDescription("OFFLINE ❌")
	.setTimestamp(new Date());

export const embedMain = new Discord.MessageEmbed()
	.setColor("#30c1ff")
	.setAuthor(
		mainServer,
		"https://cdn.discordapp.com/attachments/777244698393575444/784821735425966090/rudit.png"
	)
	.setTitle("Server ip:")
	.setDescription("main.prestiznimc.space");

export const embedBuild = new Discord.MessageEmbed()
	.setColor("#30c1ff")
	.setAuthor(
		buildServer,
		"https://cdn.discordapp.com/attachments/777244698393575444/786145178041581588/logo_64x64.png"
	)
	.setTitle("Server ip:")
	.setDescription("build.prestiznimc.space");

export const embedBois = new Discord.MessageEmbed()
	.setColor("#30c1ff")
	.setAuthor(
		boisServer,
		"https://cdn.discordapp.com/attachments/777244698393575444/784820273500127232/server-icon.png"
	)
	.setTitle("Server ip:")
	.setDescription("bois.prestiznimc.space");

export const embedHelp = new Discord.MessageEmbed()
	.setColor("#30c1ff")
	.setAuthor(
		help,
		"https://cdn.discordapp.com/attachments/777244698393575444/786159981417463858/samurai.jpg"
	)
	.setTitle("Commands:").setDescription(`.mainip
.boisip
.buildip`);
