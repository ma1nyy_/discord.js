import { prefix } from "../config.mjs";
import { embedBois, embedBuild, embedHelp, embedMain } from "../embeds.mjs";

export const textovka = (
	/** @type {Discord.Client} */
	client
) => {
	client.on("message", async (message) => {
		if (message.content === "test") {
			const sent = await message.channel.send("Test works!");

			sent.react("👌");
		}
		if (message.content === "ping") {
			message.channel.send("pong");
		}
		if (message.content === "apeks?") {
			message.channel.send("Naser si zmrde, pojď MINECRAFT!");
		}
		if (message.content === `${prefix}buildip`) {
			message.channel.send(embedBuild);
		}
		if (message.content === `${prefix}mainip`) {
			message.channel.send(embedMain);
		}
		if (message.content === `${prefix}boisip`) {
			message.channel.send(embedBois);
		}
		if (message.content === `${prefix}help`) {
			message.channel.send(embedHelp);
		}
	});
	client.on("guildMemberAdd", (member) => {
		const channel = member.guild.channels.cache.find(
			(ch) => ch.id === "675040441355468812"
		);
		if (!channel) return;
		channel.send(`WELCOME TO THE BOIS! <@${member.user.id}>`);
		member.roles.add(
			member.guild.roles.cache.find((r) => r.id === "764178051188916244")
		);
	});
};
