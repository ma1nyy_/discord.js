import * as googleTTS from "google-tts-api";
import Discord, { Message, VoiceChannel } from "discord.js";
import { prefix } from "../../config.mjs";
import { getRandomArrayElement } from "../../utils.mjs";
import { hlasky } from "./hlasky.mjs";
import { url } from "inspector";

const alfy = ["{autor}", "{autor} násilník", "{autor} predátor"];

export const tts = (
	/** @type {Discord.Client} */
	client
) => {
	client.on("message", async (message) => {
		if (message.content.startsWith(`${prefix}tts`)) {
			await schedule(message, message.content.replace(/.*? /, ""));
		}
		if (
			message.content === `${prefix}leave` ||
			message.content === `${prefix}dis`
		) {
			queue.splice(0, queue.length);
			connection?.disconnect();
			message.react("👋");
		}
		if (message.content === `${prefix}stop`) {
			queue.splice(0, queue.length);
			streamDispatcher.end();
			message.react("🛑");
		}
		if (message.content === `${prefix}hlaska`) {
			await say(
				message,
				getRandomArrayElement(hlasky).replace(
					/\{jmeno\}/g,
					getRandomArrayElement(alfy).replace(
						/\{autor\}/g,
						message.member.nickname ?? message.member.displayName
					)
				)
			);
		}
	});
};

const getAudioUrls = (text) => {
	return googleTTS
		.getAllAudioUrls(text, {
			lang: "cs-CZ",
			slow: false,
			host: "https://translate.google.com",
		})
		.map((urls) => urls.url);
};

const queue = [];
let klic = undefined;
/** @type {Discord.wVoiceConnection} */
let connection = undefined;
/** @type {Discord.StreamDispatcher} */
let streamDispatcher = undefined;

const schedule = async (message, text) => {
	const isAnythingPlaying = queue.length > 0;
	queue.push(text);
	if (isAnythingPlaying) {
	} else {
		while (true) {
			const coPraveRict = queue[0];
			console.log(`saying: ${coPraveRict}, queue: `, queue);
			await say(message, coPraveRict);
			queue.shift();
			if (queue.length == 0) break;
		}
	}
};

const say = (
	/** @type {Message} */
	message,
	text
) =>
	new Promise((resolve, reject) => {
		(async () => {
			const urls = getAudioUrls(text);

			const guild = message.guild;
			const channel = guild.channels.cache.find((channel) => {
				return (
					channel instanceof VoiceChannel &&
					channel.members.some(
						(member) => member.id == message.member.id
					)
				);
			});

			if (channel instanceof VoiceChannel) {
				connection = await channel.join();
				if (klic != undefined) clearTimeout(klic);

				const playIndex = (index) => {
					const url = urls[index];
					streamDispatcher = connection
						.play(url)
						.on("speaking", (speaking) => {
							if (!speaking) {
								if (index + 1 >= urls.length) {
									const disconnect = () => {
										console.log("disconnecting");
										connection.disconnect();
									};
									klic = setTimeout(disconnect, 20000);
									resolve();
								} else {
									playIndex(index + 1);
								}
							}
						});
				};

				playIndex(0);
			}
		})().catch(reject);
	});
