import McUtil from "minecraft-server-util";
import Discord, { TextChannel } from "discord.js";
import { embedOnline, embedOffline } from "../embeds.mjs";
import { watchedServer, guildId, channelId, pingUserId } from "../config.mjs";

let laststatus = undefined;
/** @type {Discord.Message} */
let lastmessage = undefined;

export const mc = (
	/** @type {Discord.Client} */
	client
) => {
	client.on("ready", async () => {
		const guild = client.guilds.cache.find((gu) => gu.id === guildId);
		const channel = guild.channels.cache.find((ch) => ch.id === channelId);
		if (channel instanceof TextChannel) {
			const messages = await channel.messages.fetch({});
			messages.forEach(async (m) => {
				if (m.embeds.some((e) => e.title === "Server status")) {
					await m.delete();
				}
			});
		}

		check();
	});

	const check = async () => {
		let online = false;
		try {
			const status = await McUtil.status(watchedServer);
			online = status.description.descriptionText.includes("ONLINE");
		} catch {}

		const guild = client.guilds.cache.find((g) => g.id === guildId);
		const channel = guild.channels.cache.find((ch) => ch.id === channelId);

		if (channel instanceof TextChannel) {
			if (laststatus !== online) {
				if (online) {
					if (lastmessage === undefined) {
						const message = await channel.send(embedOnline);

						lastmessage = message;
					} else {
						lastmessage.edit(embedOnline);
					}
				} else {
					const franta = client.users.cache.find(
						(f) => f.id === pingUserId
					);
					franta.send(
						"NEJDE TI SERVER TY MAGORE!!\nCHCI HRÁT MINECRAFT!!"
					);
					if (lastmessage === undefined) {
						const message = await channel.send(embedOffline);

						lastmessage = message;
					} else {
						lastmessage.edit(embedOffline);
					}
				}
			}
			laststatus = online;
		}

		setTimeout(check, 60_000);
	};
};
